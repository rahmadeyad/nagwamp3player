//
//  PlayerViewController.swift
//  Nagwa Mp3 Player
//
//  Created by Ahmad Eyad on 28/02/2022.
//

import UIKit
import AVFoundation

class PlayerViewController: UIViewController {

    public var position: Int = 0
    public var songs: [Item] = []
    
    @IBOutlet var holder: UIView!
    @IBOutlet var songNameLabel: UILabel!
    @IBOutlet var elapsedDurationLabel: UILabel!
    @IBOutlet var remainingDurationLabel: UILabel!
    @IBOutlet var playPauseButton: UIButton!
    @IBOutlet var previousButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var progressSlider: UISlider!
    
    private var player: AVAudioPlayer!
    private var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
        
        let iconsConfig = UIImage.SymbolConfiguration(pointSize: 25, weight: .light)
        previousButton.setImage(UIImage(systemName: "chevron.backward.2", withConfiguration: iconsConfig), for: .normal)
        nextButton.setImage(UIImage(systemName: "chevron.forward.2", withConfiguration: iconsConfig), for: .normal)
    }
    
    private func configure() {
        let song = songs[position]
        do {
            player = try AVAudioPlayer(contentsOf: song.path)
            guard let player = player else {
                return
            }
            player.play()
        }
        catch {
            print(error.localizedDescription)
        }
        let displayLink = CADisplayLink(target: self, selector: #selector(updateProgressSlider))
        displayLink.add(to: .current, forMode: .common)
        player.delegate = self
        // Disable previous button based on the availability of the previous file in the playlist
        if position > 0 && songs[position-1].name.contains(".mp3") {
            previousButton.isEnabled = true
        } else {
            previousButton.isEnabled = false
        }
        // Disable previous button based on the availability of the next file in the playlist
        nextButton.isEnabled = self.position < songs.count - 1 ? true : false
        // Displays the curent file name without the file extension
        songNameLabel.text = song.name.replacingOccurrences(of: ".mp3", with: "")
        progressSlider.value = 0
        progressSlider.maximumValue = Float(song.duration)
    }
    
    // Update progress slider based on audio file status
    @objc func updateProgressSlider() {
        progressSlider.value = Float(player.currentTime)
        elapsedDurationLabel.text = "\(Double(player.currentTime).formatAsTime())"
        remainingDurationLabel.text = "-\(Double(player.duration - player.currentTime).formatAsTime())"
        playPauseButton.setImage(UIImage(systemName: player.isPlaying ? "pause.fill" : "play.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 35, weight: .semibold)), for: .normal)
    }
    
    @IBAction func playPauseButtonAction(_ sender: Any) {
        if player.isPlaying {
            player.stop()
        } else {
            player.play()
        }
    }
    
    @IBAction func previousButtonAction(_ sender: Any) {
        position -= 1
        configure()
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        position += 1
        configure()
    }
    
    // Update audio file based on progress slider (user input)
    @IBAction func progressSliderAction(_ sender: UISlider) {
        player.pause()
        player.currentTime = TimeInterval(sender.value)
        if !sender.isTracking {
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.player.play()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // Stop music player on dismissing the view controller
        if player.isPlaying {
            player.stop()
        }
    }
    
}

extension PlayerViewController: AVAudioPlayerDelegate {
    // Play next song in the same playlist after current song ends
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if self.position < songs.count - 1 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.position += 1
                self.configure()
            }
        }
    }
}
