//
//  ListViewController.swift
//  Nagwa Mp3 Player
//
//  Created by Ahmad Eyad on 28/02/2022.
//

import UIKit
import AVKit

class ListViewController: UIViewController {
    
    public var items: [Item] = []
    public var path: URL!
    public var navigationTitle: String!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessageLabel: UILabel!
    
    private var fileURLs: [URL] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Navigation controller title
        self.title = navigationTitle
        // Access folder directory contents
        let fileManager = FileManager.default
        do {
            fileURLs = try fileManager.contentsOfDirectory(at: path, includingPropertiesForKeys: nil)
            for url in fileURLs {
                let name = url.lastPathComponent
                if !name.contains(".") {
                    if hasMp3FilesIn(directory: url) {
                        items.append(Item(name: name, type: name.contains(".") ? 1 : 0, duration: 0, path: url))
                    }
                } else if name.contains(".mp3") {
                    let audioPlayer = try AVAudioPlayer(contentsOf: url)
                    items.append(Item(name: name, type: name.contains(".") ? 1 : 0, duration: Double(audioPlayer.duration), path: url))
                }
            }
        } catch {
            print(error.localizedDescription)
        }
        // The items will not be empty, but it will be safer if check it
        if !items.isEmpty {
            initializeTableView()
        } else {
            tableView.isHidden = true
            emptyMessageLabel.isHidden = false
            emptyMessageLabel.text = "This folder doesn't contains mp3 files!"
        }
    }
    
    private func initializeTableView() {
        tableView.isHidden = false
        emptyMessageLabel.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        registerTableViewCell()
    }
    
    private func registerTableViewCell() {
        let itemCell = UINib(nibName: "ItemTableViewCell", bundle: nil)
        tableView.register(itemCell, forCellReuseIdentifier: "ItemTableViewCell")
    }
    
}

extension ListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell", for: indexPath) as? ItemTableViewCell {
            items.sort {
                $0.name.lowercased() < $1.name.lowercased()
            }
            items.sort {
                $0.type < $1.type
            }
            cell.titleLabel?.text = items[indexPath.row].name
            cell.durationLabel?.text = items[indexPath.row].type == 1 ? items[indexPath.row].duration.formatAsTime() : ""
            cell.iconImageView?.image = UIImage(systemName: items[indexPath.row].name.contains(".mp3") ? "music.quarternote.3" : "folder.fill")
            cell.iconImageView?.tintColor = UIColor.init(named: items[indexPath.row].type == 0 ? "folderColor" : "mp3Color")
            return cell
        }
        return UITableViewCell()
    }
}

extension ListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let position = indexPath.row
        if items[indexPath.row].type == 0 {
            guard let listViewController = storyboard?.instantiateViewController(withIdentifier: "list") as? ListViewController else {
                return
            }
            listViewController.path = items[indexPath.row].path
            listViewController.navigationTitle = items[indexPath.row].name
            navigationController?.pushViewController(listViewController, animated: true)
        } else {
            guard let playerViewController = storyboard?.instantiateViewController(withIdentifier: "player") as? PlayerViewController else {
                return
            }
            playerViewController.songs = items
            playerViewController.position = position
            present(playerViewController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
