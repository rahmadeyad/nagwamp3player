//
//  ViewController.swift
//  Nagwa Mp3 Player
//
//  Created by Ahmad Eyad on 28/02/2022.
//

import UIKit
import AVKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessageLabel: UILabel!
    // Declare an array to store contents of documents folder
    private var items:[Item] = [Item]()
    // Declare an array to store Urls of documents folder contents
    private var contentsURLs: [URL] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Navigation controller title
        self.title = "Documents"
        // Access documents directory
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
        do {
            contentsURLs = try fileManager.contentsOfDirectory(at: documentsURL!, includingPropertiesForKeys: nil)
            // Loop on the documents folder contents to store the folders and mp3 files only
            for url in contentsURLs {
                let name = url.lastPathComponent
                // find the Mp3 folders
                if !name.contains(".") {
                    if hasMp3FilesIn(directory: url) {
                        items.append(Item(name: name, type: 0, duration: 0, path: url))
                    }
                    // find the mp3 files only
                } else if name.contains(".mp3") {
                    let audioPlayer = try AVAudioPlayer(contentsOf: url)
                    items.append(Item(name: name, type: 1, duration: Double(audioPlayer.duration), path: url))
                }
            }
            // Check if items is empty to show empty message
            if items.isEmpty {
                tableView.isHidden = true
                emptyMessageLabel.isHidden = false
                emptyMessageLabel.text = "The app directory folder does not contains folders or mp3 files,\nyou can copy folders and mp3 files from your mac using finder, or from iphone files app or other apps on your phone"
            } else {
                // Hide empty message and show tableview if items isn't empty
                initializeTableView()
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func initializeTableView() {
        tableView.isHidden = false
        emptyMessageLabel.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        registerTableViewCell()
    }
    
    private func registerTableViewCell() {
        let itemCell = UINib(nibName: "ItemTableViewCell", bundle: nil)
        tableView.register(itemCell, forCellReuseIdentifier: "ItemTableViewCell")
    }
    
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell", for: indexPath) as? ItemTableViewCell {
            // Sort alphabetically with ignoring characters case
            items.sort {
                $0.name.lowercased() < $1.name.lowercased()
            }
            // Folders on top
            items.sort {
                $0.type < $1.type
            }
            cell.titleLabel?.text = items[indexPath.row].name
            cell.durationLabel?.text = items[indexPath.row].type == 1 ? items[indexPath.row].duration.formatAsTime() : ""
            cell.iconImageView?.image = UIImage(systemName: items[indexPath.row].name.contains(".mp3") ? "music.quarternote.3" : "folder.fill")
            cell.iconImageView?.tintColor = UIColor.init(named: items[indexPath.row].type == 0 ? "folderColor" : "mp3Color")
            return cell
        }
        return UITableViewCell()
    }
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let position = indexPath.row
        // Folder tap action
        if items[indexPath.row].type == 0 {
            guard let listViewController = storyboard?.instantiateViewController(withIdentifier: "list") as? ListViewController else {
                return
            }
            listViewController.path = items[indexPath.row].path
            listViewController.navigationTitle = items[indexPath.row].name
            navigationController?.pushViewController(listViewController, animated: true)
        } else {
            // Mp3 file tap action
            guard let playerViewController = storyboard?.instantiateViewController(withIdentifier: "player") as? PlayerViewController else {
                return
            }
            playerViewController.songs = items
            playerViewController.position = position
            present(playerViewController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
