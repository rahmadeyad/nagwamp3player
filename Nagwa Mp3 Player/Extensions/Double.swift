//
//  Double.swift
//  Nagwa Mp3 Player
//
//  Created by Ahmad Eyad on 01/03/2022.
//

import Foundation

extension Double {
    // Function to convert number of seconds to readable time
    func formatAsTime() -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = self >= 3600 ? [.hour, .minute, .second] : [.minute, .second]
        formatter.unitsStyle = .positional
        formatter.zeroFormattingBehavior = .pad
        return formatter.string(from: self) ?? ""
    }
}
