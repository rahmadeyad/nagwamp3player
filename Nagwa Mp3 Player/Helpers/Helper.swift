//
//  Helper.swift
//  Nagwa Mp3 Player
//
//  Created by Ahmad Eyad on 03/03/2022.
//

import Foundation

// Function to detect whether a directory folder or its subfolders contains mp3 files
func hasMp3FilesIn(directory: URL) -> Bool {
    let fileManager = FileManager.default
    do {
        let filesUrls = try fileManager.contentsOfDirectory(at: directory, includingPropertiesForKeys: nil)
        for url in filesUrls {
            if url.lastPathComponent.contains(".mp3") {
                return true
            } else if !url.lastPathComponent.contains(".") {
                if hasMp3FilesIn(directory: url) {
                    return true
                }
            }
        }
    }
    catch {
        print(error.localizedDescription)
    }
    return false
}
