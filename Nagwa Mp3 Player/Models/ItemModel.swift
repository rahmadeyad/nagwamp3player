//
//  ItemModel.swift
//  Nagwa Mp3 Player
//
//  Created by Ahmad Eyad on 02/03/2022.
//

import Foundation

struct Item {
    let name: String
    let type: Int
    let duration: Double
    let path: URL
}
