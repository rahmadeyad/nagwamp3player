//
//  Nagwa_Mp3_PlayerTests.swift
//  Nagwa Mp3 PlayerTests
//
//  Created by Ahmad Eyad on 28/02/2022.
//

import XCTest
@testable import Nagwa_Mp3_Player

class Nagwa_Mp3_PlayerTests: XCTestCase {
    
    func testDurationText() {
        var duration = 2000.formatAsTime()
        XCTAssertEqual(duration.count, 5)
        duration = 3600.formatAsTime()
        XCTAssertEqual(duration.count, 8)
        duration = 3601.formatAsTime()
        XCTAssertEqual(duration.count, 8)
    }

}
